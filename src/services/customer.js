import { BACK_URL } from "../config/configs";

const CUSTOMER_URL = `${BACK_URL}/client`
const PROVINCES_URL = `${BACK_URL}/provinces`
const LOCALITIES_URL = `${BACK_URL}/localities`
const CUSTOMERS_URL = `${BACK_URL}/clients`
const CATEGORIES_URL = `${BACK_URL}/categories`
const DOCUMENT_TYPES_URL = `${BACK_URL}/documents/types`

const listIdentificationTypes = () => {
    return new Promise((resolve, reject) => {
        fetch(DOCUMENT_TYPES_URL).then(resp => {
            if (resp.status === 204) {
                resolve([])
                return
            }
            resp.json().then(body => {
                resolve(body);
            })
        })
    });
}

const listAfipCategories = () => {
    return new Promise((resolve, reject) => {
        resolve([
            {
                id: 1,
                name: "Consumidor Final",
                isDefault: true
            }, {
                id: 2,
                name: "Responsable Inscripto"
            }, {
                id: 3,
                name: "Monotributista"
            }
        ]);
    });
}

const listStates = () => {
    return new Promise((resolve, reject) => {
        fetch(PROVINCES_URL).then(resp => {
            if (resp.status === 204) {
                resolve([])
                return
            }
            resp.json().then(body => {
                resolve(body);
            })
        })
    });
}

const listCities = (id) => {
    return new Promise((resolve, reject) => {
        fetch(`${LOCALITIES_URL}/${id}`).then(resp => {
            if (resp.status === 204) {
                resolve([])
                return
            }
            resp.json().then(body => {
                resolve(body);
            })
        })
    });
}

const listStatus = () => {
    return new Promise((resolve, reject) => {
        resolve([
            {
                id: 1,
                name: "ACTIVO",
                isDefault: true
            }, {
                id: 2,
                name: "INACTIVO"
            }, {
                id: 3,
                name: "MOROSO"
            }
        ]);
    });
}

const listCustomers = () => {
    return new Promise((resolve, reject) => {
        fetch(CUSTOMERS_URL).then(resp => {
            if (resp.status === 204) {
                resolve([])
                return
            }
            resp.json().then(body => {
                resolve(body);
            })
        }).catch(reason => {
            reject(reason)
        })
    });
}


const findCustomer = (id) => {
    return new Promise((resolve, reject) => {
        fetch(`${CUSTOMER_URL}/${id}`).then(resp => {
            resp.json().then(body => {
                resolve(body);
            })
        })
    });
}

const saveCustomer = async (name, lastName, creditLimit, email, phone, afipCategory, idType, identification, street, streetNumber, city, zipCode) => {
    // console.log(`Persist customer ${name} ${lastName} with ${idType} ${identification} `);
    return new Promise((resolve, reject) => {

        const data = {
            name: name,
            surname: lastName,
            credit_limit: creditLimit,
            email: email,
            telephone: phone,
            category_afip_id: afipCategory,
            document_type_id: idType,
            document_number: identification,
            street: street,
            street_number: streetNumber,
            locality_id: parseInt(city),
        }
        var jsonBody = JSON.stringify(data)
        console.log(jsonBody);

        fetch(`${CUSTOMER_URL}`, {
            body: jsonBody,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(res => {
            if (!res.ok) {
                res.json().then(result => {
                    reject(result.message)
                })
            } else {
                resolve(true)
            }
        }).catch(reason => {
            console.log(`Failed to create customer: ${reason.message}`);
            reject(reason.message)
        })
    })
}

const updateCustomer = (name, lastName, creditLimit, email, phone, afipCategory, idType, identification, street, streetNumber, city, zipCode) => {
    // console.log(`Persist customer ${name} ${lastName} with ${idType} ${identification} `);

    const data = {
        name: name,
        surname: lastName,
        credit_limit: creditLimit,
        email: email,
        phone: phone,
        afip_category: afipCategory,
        idType: idType,
        identification: identification,
        street: street,
        street_number: streetNumber,
        city: city,
        zip_code: zipCode
    }
    var jsonBody = JSON.stringify(data)
    console.log(jsonBody);

    /*
    fetch(`${CUSTOMER_URL}`, {
        body: jsonBody,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
    })*/
}

export { listStatus, listAfipCategories, listIdentificationTypes, listStates, listCities, saveCustomer, updateCustomer, findCustomer, listCustomers }
