import { BACK_URL } from "../config/configs";

const PRODUCTS_URL = `${BACK_URL}/products`

const listProducts = () => {
    return new Promise((resolve, reject) => {
        fetch(PRODUCTS_URL).then(resp => {
            if (resp.status === 204) {
                resolve([])
                return
            }
            resp.json().then(body => {
                resolve(body);
            })
        }).catch(reason => {
            reject(reason)
        })
    });
}

export { listProducts }