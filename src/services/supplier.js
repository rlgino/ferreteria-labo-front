import { BACK_URL } from "../config/configs";

const SUPPLIER_URL = `${BACK_URL}/supplier`
const SUPPLIERS_URL = `${BACK_URL}/suppliers`

const saveSupplier = (name, address, email, phone) => {
    console.log(`Save supplier ${name} with email ${email} and phone ${phone} with address in ${address} in ${SUPPLIER_URL}`);
    return new Promise((resolve, reject) => {
        const data = {
            name: name,
            email: email,
            address: address,
            telephone: phone,
            state: 'ACTIVE'
        }
        fetch(SUPPLIER_URL, {
            body: JSON.stringify(data),
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then((resp) => {
            console.log(`Response from supplier ${resp.status}`);
            resolve(null)
        }).catch(reason => {
            console.log(`Failed creating supplier: ${reason}`);
            reject(reason)
        })
    });
}

const listSuppliers = () => {
    return new Promise((resolve, reject) => {
        fetch(SUPPLIERS_URL).then(resp => {
            if (resp.status === 204) {
                resolve([])
                return
            }
            resp.json().then(body => {
                resolve(body);
            })
        }).catch(reason => {
            reject(reason)
        })
    });
}

const findSupplier = (id) => {
    return new Promise((resolve, reject) => {
        fetch(`${SUPPLIER_URL}/${id}`).then(resp => {
            resp.json().then(body => {
                resolve(body);
            })
        }).catch(reason => {
            reject(reason)
        })
    });
}

export { saveSupplier, listSuppliers, findSupplier }