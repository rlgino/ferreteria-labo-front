import React, { useEffect, useState } from 'react'
import { UseCart } from '../context/cart'

function ProductItem({ prod }) {
    const [quantity, setQuantity] = useState(0)

    const { addItem, removeItem, removeFromCart, totalAmount } = UseCart()

    useEffect(() => {
        if (totalAmount === 0)
            setQuantity(0)
        return () => { }
    }, [totalAmount])

    const addProduct = (id, price) => {
        const newQuantity = quantity + 1
        setQuantity(newQuantity)
        addItem(id, prod.name, 1, price)
    }

    const removeProduct = (id, price) => {
        if (quantity == 0) {
            return
        }

        const newQuantity = quantity - 1
        setQuantity(newQuantity)
        if (newQuantity === 0) {
            setQuantity(newQuantity)
            removeFromCart(id, 1, price)
        } else {
            removeItem(id, prod.name, 1, price)
        }
    }
    return (
        <div>
            <div className="row">
                <div className="col-7">
                    <p class="h3">
                        {prod.name}
                    </p>
                    <p class="lead">
                        {prod.description}
                    </p>
                </div>
                <div className="col-2">
                    <div className="row">
                        <button type="button" class="btn btn-primary col-3" onClick={e => addProduct(prod.id, prod.price)}>+</button>
                        <input type="number" class=" col-6" min="0" disabled value={quantity} />
                        <button type="button" class="btn btn-danger col-3" onClick={e => removeProduct(prod.id, prod.price)}>-</button>
                    </div>
                </div>
                <div className="col text-end">
                    <p class="h2">
                        ${prod.price}
                    </p>
                </div>
            </div>
        </div>
    )
}

export default ProductItem
