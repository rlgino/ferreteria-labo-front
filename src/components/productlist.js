import React, { useEffect, useState } from 'react'
import { listProducts } from '../services/products'
import ProductItem from './productitem'

function ProductList() {
    const [productList, setProductList] = useState([])
    const [errorMsg, setErrorMsg] = useState(null)

    useEffect(() => {
        listProducts().then(res => {
            setProductList(res)
        }).catch(reason => {
            console.error(reason);
            setErrorMsg("Error al encontrar los productos")
        })
        return () => { }
    }, [])

    return (
        <div>
            <h1>Productos</h1>
            {
                errorMsg ?
                    <div class="alert alert-danger" role="alert">
                        {errorMsg}
                    </div>
                    : <></>
            }
            <ul class="list-group">
                {
                    productList.map((prod, i) => {
                        return (
                            <li class="list-group-item" key={i}>
                                <ProductItem prod={prod} />
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}

export default ProductList
