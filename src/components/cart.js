import React, { useEffect } from 'react'
import { UseCart } from '../context/cart'

function Cart() {
    const { itemsList, totalAmount } = UseCart()

    return (
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Precio</th>
                    <th scope="col">Cantidad</th>
                    <th scope="col">Precio unitario</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
                {
                    itemsList.map((item, i) => {
                        return (
                            <tr key={i}>
                                <th scope="row">{item.name}</th>
                                <td>{item.quantity}</td>
                                <td>${item.unitPrice}</td>
                                <td>${item.quantity * item.unitPrice}</td>
                            </tr>
                        )
                    })
                }
                <tr class="table-primary">
                    <th scope="row">Total</th>
                    <td colspan="2"></td>
                    <td>${totalAmount}</td>
                </tr>
            </tbody>
        </table>
    )
}

export default Cart
