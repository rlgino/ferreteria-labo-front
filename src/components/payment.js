import React, { useState } from 'react'
import CreditCard from './sale/creditcard'

const CHECKING_ACCOUNT = "checking_acount"
const CASH = "cash"
const CREDIT_CART = "credit_card"

function PaymentData() {
    const [payment, setPayment] = useState(CASH)

    return (
        <div>
            <h4 class="mb-3">Payment</h4>

            <div className="row">
                <div class="col-3">
                    <div class="form-check">
                        <input id="credit" name="paymentMethod" type="radio" class="form-check-input" checked={payment == CASH} required onClick={e => setPayment(CASH)} />
                        <label class="form-check-label" for="credit">Efectivo</label>
                    </div>
                    <div class="form-check">
                        <input id="debit" name="paymentMethod" type="radio" class="form-check-input" checked={payment == CREDIT_CART} required onClick={e => setPayment(CREDIT_CART)} />
                        <label class="form-check-label" for="debit">Tarjeta de crédito</label>
                    </div>
                    <div class="form-check">
                        <input id="paypal" name="paymentMethod" type="radio" class="form-check-input" checked={payment == CHECKING_ACCOUNT} required onClick={e => setPayment(CHECKING_ACCOUNT)} />
                        <label class="form-check-label" for="paypal">Cuenta corriente</label>
                    </div>
                </div>

                {
                    payment === CREDIT_CART ? <CreditCard /> : <></>
                }
            </div>
        </div>
    )
}

export default PaymentData
