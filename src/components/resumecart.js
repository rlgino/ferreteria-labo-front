import React from 'react'
import { useHistory } from 'react-router';
import { UseCart } from '../context/cart'
import Cart from './cart'

function ResumeCart() {
    let history = useHistory();
    const { totalAmount, clearCart, saveIntoStorage } = UseCart()

    const continueToSale = () => {
        saveIntoStorage().then(() => {
            history.push("/customersale")
        })
    }

    return (
        <div>
            <div>
                <h1>Compra</h1>
                <Cart />
                <div className="row d-flex d-row justify-content-around">
                    <button class={"btn btn-primary col-4" + (totalAmount === 0 ? " disabled" : "")} onClick={e => continueToSale()}>Pasar al pago</button>
                    <button type="button" class="btn btn-danger col-4" onClick={e => clearCart()}>Rechazar compra</button>
                </div>
            </div>
        </div >
    )
}

export default ResumeCart
