import React from 'react'
import { useHistory } from 'react-router'

function SaleActions() {
    var history = useHistory()
    const saveSale = () => { }

    const clearSale = () => {
        localStorage.removeItem("ferreteria-cust")
        localStorage.removeItem("ferreteria-cart")
        localStorage.removeItem("ferreteria-total")
        history.push("/sale")
    }

    return (
        <div className="d-flex flex-column align-self-stretch">
            <button className={"w-100 btn btn-primary btn-lg"} onClick={e => saveSale()}>Finalizar venta</button>
            <button className={"w-100 btn btn-danger btn-lg mt-3"} onClick={e => clearSale()}>Cancelar venta</button>
        </div>
    )
}

export default SaleActions
