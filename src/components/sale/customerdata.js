import React, { useEffect, useState } from 'react'

function CustomerData() {
    const [name, setName] = useState("Gino")
    const [lastName, setLastName] = useState("Luraschi")
    const [document, setDocument] = useState("12341234")
    const [address, setAddress] = useState("Av Siempre Viva 123")
    const [billType, setBillType] = useState("A")

    useEffect(() => {
        var myJSON = localStorage.getItem("ferreteria-cust")
        var obj = JSON.parse(myJSON)
        if (!obj) return
        setName(obj.name)
        setLastName(obj.lastName)
        setDocument(obj.document)
        setAddress(obj.address)
        return () => { }
    }, [])

    return (
        <div>
            <h3>Cliente</h3>

            <div className="col-md-6">Nombre: {name} {lastName} - {document}</div>
            <div className="col-md-6">Domicilio: {address}</div>
            <div className="col-md-6">
                <label for="bill-type" className="form-label" value={billType} onClick={e => setBillType(e.target.value)}>Tipo de factura</label>
                <select className="form-select" id="bill-type" required>
                    <option value="">Elija...</option>
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="C">C</option>
                </select>
            </div>
        </div>
    )
}

export default CustomerData
