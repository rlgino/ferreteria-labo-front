import React from 'react'

function CreditCard() {
    return (
        <div class="col-6 row">
            <div class="col-md-6">
                <label for="cc-name" class="form-label">Nombre</label>
                <input type="text" class="form-control" id="cc-name" placeholder="" required />
                <small class="text-muted">Como aparece en la tarjeta</small>
                <div class="invalid-feedback">
                    Nombre de la tarjeta es requerido
                                </div>
            </div>

            <div class="col-md-6">
                <label for="cc-number" class="form-label">Número de tarjeta</label>
                <input type="text" class="form-control" id="cc-number" placeholder="" required />
                <div class="invalid-feedback">
                    Numero de la tarjeta es requerido
                                </div>
            </div>

            <div class="col-md-3">
                <label for="cc-expiration" class="form-label">Vencimiento</label>
                <input type="text" class="form-control" id="cc-expiration" placeholder="" required />
                <div class="invalid-feedback">
                    Fecha de vencimiento requerido
                                </div>
            </div>

            <div class="col-md-3">
                <label for="cc-cvv" class="form-label">CVV</label>
                <input type="text" class="form-control" id="cc-cvv" placeholder="" required />
                <div class="invalid-feedback">
                    Security code required
                                </div>
            </div>
        </div>
    )
}

export default CreditCard
