const BACK_URL = process.env.REACT_APP_URL_BACK || 'https://heroku-tp-pp1-back-qa.herokuapp.com/api/v1'

export { BACK_URL }