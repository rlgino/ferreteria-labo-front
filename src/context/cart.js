import { createContext, useContext, useMemo, useState } from 'react'

const Cart = createContext()

export function CartProvider(props) {
    const [itemsList, setItemsList] = useState([])
    const [totalAmount, setTotalAmount] = useState(0)

    const saveIntoStorage = async () => {
        var myJSON = JSON.stringify(itemsList)
        console.log(myJSON);
        localStorage.setItem("ferreteria-cart", myJSON)
        localStorage.setItem("ferreteria-total", totalAmount)
    }

    const loadFromStorage = async () => {
        return new Promise((resolve, reject) => {
            var myJSON = localStorage.getItem("ferreteria-cart")
            var obj = JSON.parse(myJSON)
            setItemsList(obj)
            if (!obj) {
                reject("Empty cart")
            }
            var totalJson = localStorage.getItem("ferreteria-total")
            setTotalAmount(totalJson)
            if (!totalJson) {
                reject("Empty total")
            }
        })
    }

    const addItem = (productID, name, quantity, price) => {
        const localList = itemsList

        const result = totalAmount + (parseInt(quantity) * parseFloat(price))
        setTotalAmount(parseFloat(result))

        const updatedIndex = localList.findIndex(item => item.id === productID)
        if (updatedIndex === -1) {
            const newItem = {
                id: productID,
                name: name,
                quantity: quantity,
                unitPrice: price
            }
            localList.push(newItem)
        } else {
            const newItem = localList[updatedIndex]
            newItem.quantity = newItem.quantity + 1
            localList[updatedIndex] = newItem
        }

        setItemsList(localList)
    }

    const removeItem = (productID, name, quantity, price) => {
        const localList = itemsList

        const result = totalAmount - (parseInt(quantity) * parseFloat(price))
        setTotalAmount(parseFloat(result))

        const updatedIndex = localList.findIndex(item => item.id === productID)
        if (updatedIndex === -1) {
            const newItem = {
                id: productID,
                name: name,
                quantity: quantity,
                unitPrice: price
            }
            localList.push(newItem)
        } else {
            const newItem = localList[updatedIndex]
            newItem.quantity = newItem.quantity - quantity
            localList[updatedIndex] = newItem
        }

        setItemsList(localList)
    }

    const removeFromCart = (productID, quantity, price) => {
        const result = totalAmount - (parseInt(quantity) * parseFloat(price))
        setTotalAmount(parseFloat(result))

        const localList = itemsList
        const removeIndex = localList.findIndex(item => item.id === productID)
        if (removeIndex > -1) {
            localList.splice(removeIndex, 1)
        }
        setItemsList(localList)
    }

    const clearCart = () => {
        setItemsList([]);
        setTotalAmount(0);
    }

    const value = useMemo(() => {
        return {
            itemsList, addItem, removeItem, clearCart, removeFromCart, totalAmount, loadFromStorage, saveIntoStorage
        }
    }, [itemsList, totalAmount])

    return <Cart.Provider value={value} {...props} />
}

export function UseCart() {
    const context = useContext(Cart)
    if (!context)
        throw new Error("Not cart context")

    return context
}