import React, { useEffect, useState } from 'react'
import { listSuppliers } from '../../services/supplier'

function SuppliersList() {
    const [suppliersList, setSuppliersList] = useState([])

    useEffect(() => {
        listSuppliers().then(suppliersListResp => {
            setSuppliersList(suppliersListResp)
        })
        return () => { }
    }, [])
    return (
        <div>
            <h1>Proveedores</h1>
            <a className="btn btn-primary" href="/supplier">Crear Proveedor</a>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        suppliersList.map((supplier, i) => {
                            return (
                                <tr className={supplier.state !== "ACTIVE" ? "table-danger" : ""} key={i}>
                                    <th scope="row">{supplier.id}</th>
                                    <td>{supplier.name}</td>
                                    <td>{supplier.telephone}</td>
                                    <td>
                                        <div className="row">
                                            <a href={`/supplier/${supplier.id}`}>Editar</a>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default SuppliersList
