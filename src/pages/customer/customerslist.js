import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { findCustomer, listCustomers } from '../../services/customer'

function CustomersList() {
    const [customerList, setCustomerList] = useState([])
    const [idFilter, setIdFilter] = useState("")
    const [identification, setIdentification] = useState("")
    const [emailFilter, setEmailFilter] = useState("")

    useEffect(() => {
        /*
        listCustomers().then(suppliersListResp => {
            setCustomerList(suppliersListResp)
        })
        */
        return () => { }
    }, [])

    const searchCustomer = () => {
        if (idFilter) {
            findCustomer(idFilter).then(res => {
                const newList = [res]
                setCustomerList(newList)
                setIdFilter("")
            })
        }
    }
    return (
        <div>
            <h1>Clientes</h1>
            <div className="row">
                <div className="col-1"></div>
                <div className="col-2 d-flex align-items-center">
                    <a className="btn btn-primary" href="/customer">Crear Cliente</a>
                </div>
                <div className="col d-flex justify-content-between align-items-center">
                    <div className="col-3">
                        <label for="id" className="form-label">ID</label>
                        <input type="number" className="form-control" id="id" value={idFilter} onChange={e => { setIdFilter(e.target.value) }} required disabled={identification || emailFilter} />
                    </div>
                    <div className="col-3">
                        <label for="identification" className="form-label">Identificación</label>
                        <input type="text" className="form-control" id="identification" value={identification} onChange={e => { setIdentification(e.target.value) }} required disabled={idFilter || emailFilter} />
                    </div>
                    <div className="col-3">
                        <label for="email" className="form-label">Email</label>
                        <input type="email" className="form-control" id="email" value={emailFilter} onChange={e => { setEmailFilter(e.target.value) }} placeholder="mail@mail.com" required disabled={identification || idFilter} />
                    </div>
                    <div className="col-2">
                        <button type="button" className="btn btn-warning" disabled={!idFilter && !identification && !emailFilter} onClick={e => searchCustomer()}>Buscar Cliente</button>
                    </div>
                </div>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Mail</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Identificación</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        customerList.map((customer, i) => {
                            return (
                                <tr className={customer.state !== "ACTIVE" ? "table-danger" : ""} key={i}>
                                    <th scope="row">{customer.id}</th>
                                    <td>{customer.name} {customer.surname}</td>
                                    <td>{customer.email}</td>
                                    <td>{customer.state}</td>
                                    <td>{customer.document_number}</td>
                                    <td>
                                        <div className="row">
                                            <Link to={`/customer/${customer.id}`}>Editar</Link>
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default CustomersList
