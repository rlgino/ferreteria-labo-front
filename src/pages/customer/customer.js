import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { listIdentificationTypes, listAfipCategories, listStates, listStatus, saveCustomer, updateCustomer, listCities, findCustomer } from '../../services/customer'
import './customer.css'

function CustomerForm() {
    const [statusList, setStatusList] = useState([])
    const [afipCategories, setAfipCategories] = useState([])
    const [identificationsList, setIdentificationsList] = useState([])
    const [stateList, setStateList] = useState([])
    const [citiesList, setCitiesList] = useState([])

    const [name, setName] = useState("")
    const [lastName, setLastName] = useState("")
    const [creditLimit, setCreditLimit] = useState(0)
    const [status, setStatus] = useState(1)
    const [email, setEmail] = useState("")
    const [phone, setPhone] = useState("")

    const [category, setCategory] = useState(null)
    const [idType, setIdType] = useState(null)
    const [identification, setIdentification] = useState("")
    const [street, setStreet] = useState("")
    const [streetNumber, setStreetNumber] = useState("")
    const [city, setCity] = useState(null)
    const [state, setState] = useState(null)
    const [zipCode, setZipCode] = useState("")
    const [errorMsg, setErrorMsg] = useState(null)

    let { id } = useParams();

    useEffect(() => {
        listIdentificationTypes().then(res => {
            setIdentificationsList(res)
        })
        listAfipCategories().then(res => {
            setAfipCategories(res)
        })
        listStatus().then(res => {
            setStatusList(res)
            const i = res.findIndex(status => status.isDefault)
            setStatus(res[i].id)
        })
        listStates().then(res => {
            setStateList(res)
        })
        if (id) {
            findCustomer(id).then(res => {
                setName(res.name)
                setLastName(res.surname)
                setEmail(res.email)
                setPhone(res.telephone)
                setCreditLimit(res.credit_limit)
                setIdType(res.document_type.id)
                setIdentification(res.document_number)
                setCity(res.locality.id)
                listStates().then(states => {
                    // TODO: Find cities into states
                })
                setStreet(res.street)
                setStreetNumber(res.street_number)
                setZipCode(res.locality.zip_code)
            })
        }
        return () => { }
    }, [])

    useEffect(() => {
        setCitiesList([]);
        setCity(null)
        if (!state) {
            return
        }
        listCities(state).then(res => {
            setCitiesList(res)
        })
        return () => { }
    }, [state])

    useEffect(() => {
        if (!city) {
            setZipCode("")
            return
        }
        const i = citiesList.findIndex(cityItem => parseInt(city) === cityItem.id)
        if (i < 0) {
            setZipCode("")
            return
        }
        var zipCode = citiesList[i].zip_code
        setZipCode(zipCode)
        return () => { }
    }, [city])

    const onSubmit = (e) => {
        e.preventDefault()
        if (id) {
            updateCustomer(name, lastName, creditLimit, email, phone, category, idType, identification, street, streetNumber, city, zipCode)
            return
        }
        saveCustomer(name, lastName, creditLimit, email, phone, category, idType, identification, street, streetNumber, city, zipCode)
            .then((res) => {
                setErrorMsg(null)
                setName("")
                setLastName("")
                setPhone("")
                setEmail("")
                setCreditLimit(0)
                setEmail("")
                setIdType(null)
                setIdentification("")
                setCategory(null)
                setState(null)
                setCity(null)
                setStreet("")
                setStreetNumber("")
                setZipCode("")
            })
            .catch(reason => {
                setErrorMsg(reason)
            })
    }

    return (
        <main>
            <h3 className="mb-3">Nuevo cliente</h3>
            {
                errorMsg ?
                    <div class="alert alert-danger" role="alert" onClick={() => setErrorMsg(null)}>
                        {errorMsg}
                    </div> : <></>
            }
            <form className="needs-validation row" onSubmit={e => onSubmit(e)}>
                <div className="row col-sm-12 col-md-8 col-lg-6">
                    <div className="col-sm-6">
                        <label for="firstName" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="firstName" value={name} onChange={e => { setName(e.target.value) }} required />
                    </div>

                    <div className="col-sm-6">
                        <label for="lastName" className="form-label">Apellido</label>
                        <input type="text" className="form-control" id="lastName" value={lastName} onChange={e => { setLastName(e.target.value) }} required />
                    </div>
                </div>
                <div className="row col-sm-12 col-md-8 col-lg-6">
                    <div className="col">
                        <label for="credit-limit" className="form-label">Limite de crédito</label>
                        <div class="input-group mb-3">
                            <span class="input-group-text">$</span>
                            <input type="number" min="0" class="form-control" id="credit-limit" aria-label="Limite crediticio" value={creditLimit} onChange={e => setCreditLimit(e.target.value)} required />
                            <span class="input-group-text">.00</span>
                        </div>
                    </div>

                    <div className="col">
                        <label for="status" className="form-label">Estado</label>
                        <select className="form-select" id="status" value={status} onChange={e => { setStatus(e.target.value) }} required disabled={!id}>
                            {statusList.map(statusItem => <option value={statusItem.id}>{statusItem.name}</option>)}
                        </select>
                    </div>
                </div>
                <div className="row col-sm-12 col-md-8 col-lg-6">
                    <div className="col">
                        <label for="email" className="form-label">Email</label>
                        <input type="email" className="form-control" id="email" value={email} onChange={e => { setEmail(e.target.value) }} placeholder="mail@mail.com" required />
                    </div>
                </div>
                <div className="row col-sm-12 col-md-8 col-lg-6">
                    <div className="col-6">
                        <label for="phone" className="form-label">Telefono</label>
                        <div class="input-group mb-3">
                            <span class="input-group-text">+54 9</span>
                            <input type="string" maxlength="10" className="form-control" id="phone" value={phone} onChange={e => { setPhone(e.target.value) }} required />
                        </div>
                    </div>
                </div>

                <hr className="my-4" />

                <h4 className="mb-3">Datos para Facturación</h4>

                <div className="row col-sm-12 col-md-8 col-lg-12">
                    <div className="col-4">
                        <label for="type" className="form-label">Tipo</label>
                        <select className="form-select" id="type" value={idType} onChange={e => { setIdType(e.target.value) }} required>
                            <option value="">Elija...</option>
                            {
                                identificationsList.map(type => { return (<option value={type.id}>{type.name}</option>) })
                            }
                        </select>
                    </div>

                    <div className="col">
                        <label for="identification" className="form-label">Identificación</label>
                        <input type="text" className="form-control" id="identification" value={identification} onChange={e => { setIdentification(e.target.value) }} required />
                    </div>

                    <div className="col-4">
                        <label for="category" className="form-label">Categoria</label>
                        <select className="form-select" id="category" value={category} onChange={e => { setCategory(e.target.value) }} required>
                            <option value="">Elija...</option>
                            {afipCategories.map(categoryItem => <option value={categoryItem.id}>{categoryItem.name}</option>)}
                        </select>
                    </div>
                </div>
                <div className="row col-sm-12 col-md-8 col-lg-6">
                    <div className="col-6">
                        <label for="state" className="form-label">Provincia</label>
                        <select className="form-select" id="state" value={state} onChange={e => { setState(e.target.value || null) }} required>
                            <option value="" selected="false">Elegir</option>
                            {stateList.map(stateItem => <option value={stateItem.id}>{stateItem.name}</option>)}
                        </select>
                    </div>
                    <div className="col-6">
                        <label for="localidad" className="form-label">Localidad</label>
                        <select className="form-select" id="localidad" value={city} onChange={e => { setCity(e.target.value || null); }} disabled={state === null} required>
                            <option value="" selected="false">Elegir</option>
                            {citiesList.map(cityItem => <option value={cityItem.id}>{cityItem.name}</option>)}
                        </select>
                    </div>
                </div>
                <div className="row col-sm-12 col-md-8 col-lg-6">
                    <div className="col-6">
                        <label for="street" className="form-label">Calle</label>
                        <input type="text" className="form-control" id="street" value={street} onChange={e => { setStreet(e.target.value) }} required />
                    </div>
                    <div className="col-3">
                        <label for="street-number" className="form-label">Altura</label>
                        <input type="number" className="form-control" id="street-number" value={streetNumber} onChange={e => { setStreetNumber(e.target.value) }} required />
                    </div>
                    <div className="col-md-3">
                        <label for="zip" className="form-label">Código postal</label>
                        <input type="text" className="form-control" id="zip" value={zipCode} disabled required />
                    </div>
                </div>

                <hr className="my-4" />

                <div className="row g-3">
                    <div className="col-sm-3">
                        <button className="w-100 btn btn-primary btn-lg" type="submit">{id ? "Actualizar" : "Guardar"}</button>
                    </div>
                    <div className="col-sm-2">
                        <button className="w-100 btn btn-danger btn-lg" type="button" disabled={!id}>Eliminar</button>
                    </div>
                </div>
            </form>
        </main>
    )
}

export default CustomerForm
