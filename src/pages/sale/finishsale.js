import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router';
import Cart from '../../components/cart';
import PaymentData from '../../components/payment';
import CustomerData from '../../components/sale/customerdata';
import SaleActions from '../../components/sale/saleactions';
import { UseCart } from '../../context/cart';

function FinishSaleForm() {
    // Loading cart from Local Storage
    const { addItem, loadFromStorage } = UseCart()
    const [cartFromStorage, setCartFromStorage] = useState([])

    var history = useHistory()

    useEffect(() => {
        loadFromStorage().catch(() => {
            history.push("/sale")
        })
        return () => { }
    }, [])
    return (
        <div>
            <h1>Finalizar la venta</h1>
            <div className="row d-flex d-row justify-content-around">
                <div className="col-4">
                    <h3>Productos</h3>
                    <Cart />
                </div>
                <div className="col-4">
                    <CustomerData />
                </div>
                <div className="col-3">
                    <SaleActions />
                </div>
            </div>
            <div>
                <div>
                    <PaymentData />
                </div>
            </div>
        </div>
    )
}

export default FinishSaleForm
