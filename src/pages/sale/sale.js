import React from 'react'
import ProductList from '../../components/productlist'
import ResumeCart from '../../components/resumecart'

function SaleForm() {
    return (
        <div>
            <div className="row">
                <div className="col-8">
                    <ProductList />
                </div>
                <div className="col-4">
                    <ResumeCart />
                </div>
            </div>
        </div>
    )
}

export default SaleForm
