import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router'
import { listCustomers } from '../../services/customer'

function CustomerSaleForm() {
    const [customer, setCustomer] = useState("")
    const [errorMsg, setErrorMsg] = useState("")
    const [customerList, setCustomerList] = useState([
        {
            id: 1,
            name: "Pepe",
            surname: "Pato",
            document_number: "12341234",
            address: "Av Siempre Viva 123"
        },
        {
            id: 2,
            name: "Natalia",
            surname: "Natalia",
            document_number: "87654321",
            address: "Av Gdor Ugarte 333"
        },
        {
            id: 3,
            name: "Pechugas",
            surname: "Laru",
            document_number: "14141414",
            address: "Yrigoyen 444"
        }
    ])

    useEffect(() => {
        listCustomers().then(res => {
            setCustomerList(res)
        }).catch(err => {
            setErrorMsg(err.message)
        })
        return () => {}
    }, [])

    const history = useHistory()

    const saveCustomer = () => {
        var selectedCustomerIndex = customerList.findIndex(val => parseInt(val.id) === parseInt(customer))
        var myJSON = JSON.stringify(customerList[selectedCustomerIndex])
        localStorage.setItem("ferreteria-cust", myJSON)
        history.push("/finishsale")
    }

    return (
        <div>
            <h1>Elija el cliente</h1>
            <div className="row">
                <div className="col-6">
                    <label for="exampleDataList" className="form-label">Datalist example</label>
                    <input className="form-control" list="datalistOptions" id="exampleDataList" placeholder="Buscar cliente existente..." value={customer} onChange={e => { setCustomer(e.target.value) }} />
                    <datalist id="datalistOptions" >
                        {
                            customerList.map(val => <option value={val.id} label={`${val.document_number} - ${val.name} ${val.surname}`} />)
                        }
                    </datalist>
                </div>
                <div className="col-4">
                    <br />
                    <button className={"w-100 btn btn-primary btn-lg" + (customer ? "" : "disabled")} onClick={e => saveCustomer()}>Continuar al pago</button>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-6 d-flex d-row justify-content-center">
                    <div className="col-6">
                        <a className="w-100 btn btn-danger btn-lg" href="/sale">Volver</a>
                    </div>
                </div>
                <div className="col-4">
                    <a className="w-100 btn btn-warning btn-lg" href="/customer">Crear cliente</a>
                </div>
            </div>
        </div>
    )
}

export default CustomerSaleForm
