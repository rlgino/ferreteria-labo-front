import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router'
import { findSupplier, saveSupplier } from '../../services/supplier'

function SupplierForm() {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [address, setAddress] = useState("")
    const [phone, setPhone] = useState("")
    const [errorMsg, setErrorMsg] = useState(null)

    let history = useHistory();

    let { id: supplierID } = useParams();

    useEffect(() => {
        if (supplierID !== undefined) {
            findSupplier(supplierID).then(res => {
                setName(res.name)
                setEmail(res.email)
                setPhone(res.telephone)
                setAddress(res.address)
            })
        }
        return () => { }
    }, [])

    const onSubmit = (e) => {
        e.preventDefault()
        if (supplierID !== undefined) {
            setErrorMsg("Not implemented")
            history.push("/suppliers")
        } else {
            saveSupplier(name, address, email, phone).then(() => {
                history.push("/suppliers")
            }).catch(reason => {
                console.error(reason);
                setErrorMsg("No se pudo guardar el cliente correctamente")
            });
        }
    }

    return (
        <main>
            <h4 className="mb-3">Nuevo proveedor</h4>
            {
                errorMsg ?
                    <div class="alert alert-danger" role="alert" onClick={() => setErrorMsg(null)}>
                        {errorMsg}
                    </div> : <></>
            }
            <form className="needs-validation" className="row" onSubmit={e => onSubmit(e)}>
                <div className="col-6">
                    <div>
                        <label htmlFor="name" className="form-label">Nombre</label>
                        <input type="text" className="form-control" id="name" value={name} onChange={e => setName(e.target.value)} required />
                    </div>

                    <div>
                        <label htmlFor="address" className="form-label">Dirección</label>
                        <input type="text" className="form-control" id="address" placeholder="Calle 111, Provincia, Pais" value={address} onChange={e => setAddress(e.target.value)} required />
                    </div>
                </div>

                <div className="col-6">
                    <div>
                        <label htmlFor="email" className="form-label">Email</label>
                        <input type="email" className="form-control" id="email" placeholder="mail@mail.com" value={email} onChange={e => setEmail(e.target.value)} required />
                    </div>

                    <div>
                        <label htmlFor="phone" className="form-label">Telefono</label>
                        <input type="text" className="form-control" id="phone" placeholder="1112341234" value={phone} onChange={e => setPhone(e.target.value)} required />
                    </div>
                </div>

                <hr className="my-4" />

                <div className="row g-3">
                    <div className="col-sm-3">
                        <button className="w-100 btn btn-primary btn-lg" type="submit">{supplierID === undefined ? "Guardar" : "Actualizar"}</button>
                    </div>
                    <div className="col-sm-2">
                        <button className="w-100 btn btn-danger btn-lg" type="button">Eliminar</button>
                    </div>
                </div>
            </form>
        </main >
    )
}

export default SupplierForm
