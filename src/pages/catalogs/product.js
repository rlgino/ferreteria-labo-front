import React, { useState } from 'react'
import './product.css'
import { saveProduct } from '../../services/productService'

function ProductForm() {
    const [name, setName] = useState("")
    const [price, setPrice] = useState(0)
    const [stock, setStock] = useState(0)
    const [stockMeasure, setStockMeasure] = useState(null)

    const [havePrefeerProvider, setHavePrefeerProvider] = useState(false)
    const [prefeerProviderSelected, setPrefeerProviderSelected] = useState(null)
    const [minStock, setMinStock] = useState(0)
    const [minStockMeasure, setMinStockMeasure] = useState(null)

    const onSubmitForm = (e) => {
        e.preventDefault()
        if (price <= 0) {
            console.error("");
        }
        saveProduct(name, price, stock)
    }

    return (
        <main>
            <h4 className="mb-3">Nuevo producto</h4>
            <form className="needs-validation row" onSubmit={e => { onSubmitForm(e) }}>
                <div className="row g-3 col-6">

                    <div className="col-sm-6">
                        <label htmlFor="price" className="form-label">Precio</label>
                        <div className="input-group col-sm-6">
                            <span className="input-group-text">$</span>
                            <input id="price" type="number" min="1" className="form-control" aria-label="Amount (to the nearest dollar)" value={price} onChange={e => setPrice(e.target.value)} required />
                        </div>
                    </div>

                    <br />
                    <br />

                    <div className="col-12">
                        <label htmlFor="name" className="form-label">Nombre del producto</label>
                        <input type="text" className="form-control" id="name" placeholder="" value={name} onChange={e => setName(e.target.value)} required />
                        <div className="invalid-feedback">
                            Valid name is required.
                        </div>
                    </div>
                </div>

                <div className="col">
                    <div className="row">

                        <div className="col-6">
                            <label htmlFor="stock" className="form-label">Stock Inicial</label>
                            <input type="number" className="form-control" min="0" id="stock" placeholder="" value={stock} onChange={e => setStock(e.target.value)} required />
                            <div className="invalid-feedback">
                                Stock inválido
                            </div>
                        </div>

                        <div className="col-4">
                            <label htmlFor="type" className="form-label">Unidad de medida</label>
                            <select className="form-select" id="type">
                                <option value="">Elija...</option>
                                <option>Metros</option>
                                <option>KG</option>
                            </select>
                            <div className="invalid-feedback">
                                Elija una unidad de medida del producto
                        </div>
                        </div>
                    </div>

                    <br />

                    <div className="form-check">
                        <input type="checkbox" className="form-check-input" id="is-prefeer-provider" value={havePrefeerProvider} onClick={e => { console.log("asd"); setHavePrefeerProvider(!havePrefeerProvider) }} />
                        <label className="form-check-label" for="is-prefeer-provider">Tiene proveedor preferido</label>
                    </div>

                    <br />

                    <div className="row">

                        <div className="col-6">
                            <label htmlFor="type" className="form-label">Proveedor preferido</label>
                            <select className="form-select" id="type" disabled={!havePrefeerProvider}>
                                <option value="">Elija...</option>
                                <option>Easy</option>
                                <option>Sodimac</option>
                            </select>
                            <div className="invalid-feedback">
                                Elija un proveedor del producto
                            </div>
                        </div>

                        <div className="col-2">
                            <label htmlFor="stock" className="form-label">Stock minimo</label>
                            <input type="number" disabled={!havePrefeerProvider} className="form-control" min="1"
                                id="stock" placeholder="" value={minStock} onChange={e => setMinStock(e.target.value)} required />
                            <div className="invalid-feedback">
                                Stock minimo inválido
                            </div>
                        </div>

                        <div className="col-3">
                            <label htmlFor="type" className="form-label">Medida</label>
                            <select className="form-select" id="type" disabled={!havePrefeerProvider}>
                                <option value="">Elija...</option>
                                <option>Metros</option>
                                <option>KG</option>
                            </select>
                            <div className="invalid-feedback">
                                Elija una unidad de medida del producto
                            </div>
                        </div>
                    </div>

                </div>

                <hr className="my-4" />

                <div className="row g-3">
                    <div className="col-sm-3">
                        <button className="w-100 btn btn-primary btn-lg" type="submit">Guardar</button>
                    </div>
                    <div className="col-sm-2">
                        <button className="w-100 btn btn-danger btn-lg" type="button">Eliminar</button>
                    </div>
                </div>
            </form>
        </main>
    )
}

export default ProductForm
