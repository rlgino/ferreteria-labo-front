import './App.css';
import Navbar from './components/navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Home from './pages/home';
import Login from './pages/user/login';
import Signup from './pages/user/signup';
import CustomerForm from './pages/customer/customer';
import ProductForm from './pages/catalogs/product';
import SupplierForm from './pages/catalogs/supplier';
import SuppliersList from './pages/lists/suppliers';
import SaleForm from './pages/sale/sale';
import { CartProvider } from './context/cart';
import FinishSaleForm from './pages/sale/finishsale';
import CustomerSaleForm from './pages/sale/customersale';
import CustomersList from './pages/customer/customerslist';

function App() {
  return (
    <div className="App">
      <CartProvider>
        <Router>
          <Switch>
            <Route path="/">
              <Navbar />
              <Route exact path="/">
                <SaleForm />
              </Route>
              <Route exact path="/sale">
                <SaleForm />
              </Route>
              <Route exact path="/customersale">
                <CustomerSaleForm />
              </Route>
              <Route exact path="/finishsale">
                <FinishSaleForm />
              </Route>

              <Route exact path="/login">
                <Login />
              </Route>
              <Route exact path="/signup">
                <Signup />
              </Route>

              <Route exact path="/customer">
                <CustomerForm />
              </Route>
              <Route exact path="/customer/:id">
                <CustomerForm />
              </Route>
              <Route exact path="/customers">
                <CustomersList />
              </Route>
              <Route exact path="/suppliers">
                <SuppliersList />
              </Route>
              <Route exact path="/supplier/:id">
                <SupplierForm />
              </Route>
              <Route exact path="/supplier">
                <SupplierForm />
              </Route>
              <Route exact path="/product">
                <ProductForm />
              </Route>
            </Route>
          </Switch>
        </Router>
      </CartProvider>
    </div>
  );
}

export default App;
